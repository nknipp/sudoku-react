module.exports = {
  collectCoverage: true,
  collectCoverageFrom: ['**/src/**/*.(t|j)sx?', '!**/__tests__/**', '!**/node_modules/**'],
  // coverageThreshold: {
  //   global: {
  //     statements: 95,
  //     branches: 95,
  //     functions: 95,
  //     lines: 95
  //   }
  // },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'json', 'node'],
  roots: ['src'],
  setupFilesAfterEnv: ['./jest.setup.js'],
  testEnvironment: 'jsdom',
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$',
  testURL: 'http://localhost/',
  transform: {
    '^.+\\.(j|t)sx?$': 'babel-jest'
  },
  transformIgnorePatterns: ['node_modules']
}
