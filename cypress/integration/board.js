it('should pause the game', () => {
  cy.visit('/')

  cy.contains('Start new game').click()

  cy.get('button[title="Resume game"].disabled').should('be.visible')
  cy.get('button[title="Pause game"]').click()

  cy.contains('Game paused').should('be.visible')
})

it('should resume the game', () => {
  cy.visit('/')

  cy.contains('Start new game').click()

  cy.get('button[title="Pause game"]').click()

  cy.get('button[title="Resume game"]').click()

  cy.contains('Game paused').should('be.not.visible')
})

it('should cancel the game', () => {
  cy.visit('/')

  cy.contains('Start new game').click()

  cy.get('button[title="Cancel game"]').click()

  cy.contains('Start new game').should('be.visible')
})
