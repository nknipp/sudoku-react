module.exports = {
  'env': {
    'browser': true,
    'cypress/globals': true,
    'es2021': true
  },
  'extends': [
    'eslint:recommended',
    'plugin:cypress/recommended',
    'plugin:jest-dom/recommended',
    'plugin:react/recommended',
    'plugin:react-hooks/recommended',
    'plugin:testing-library/react',
    'plugin:@typescript-eslint/recommended'
  ],
  'parser': '@typescript-eslint/parser',
  'parserOptions': {
    'ecmaFeatures': {
      'jsx': true
    },
    'ecmaVersion': 12,
    'sourceType': 'module'
  },
  'plugins': [
    'cypress',
    'jest-dom',
    'react',
    'react-hooks',
    'testing-library',
    '@typescript-eslint'
  ],
  'rules': {
    'linebreak-style': [
      'error',
      'unix'
    ],
    'quotes': [
      'error',
      'single'
    ],
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
    'semi': [
      'error',
      'never'
    ]
  }
}
