import React from 'react'
import { css, Global } from '@emotion/core'
import { Router } from './router'
import digitalNumberfont from './assets/DigitalNumbers-Regular.woff'

const globalStyles = css`
  @font-face {
    font-family: 'Digital Numbers Regular';
    font-style: normal;
    font-weight: normal;
    src: local('Digital Numbers Regular'), url(${digitalNumberfont}) format('woff');
  }
  
  body {
    margin: 0;
  }

  body * {
    box-sizing: border-box;
  }
`

const appStyles = css`
  --board-dimension: 396px;
  
  min-height: 100vh;

  display: flex;
  justify-content: center;
  align-items: center;
  
  @media (orientation: portrait) and (max-width: 395px) {
    --board-dimension: 100vw;
  }

  @media (orientation: portrait) and (max-width: 349px) {
    --board-dimension: 360px;
    width: var(--board-dimension);
    height: unset;
  }

  @media (orientation: landscape) and (max-height: 395px) {
    --board-dimension: 100vh;
  }

  @media (orientation: landscape) and (max-height: 349px) {
    --board-dimension: 360px;
    width: 630px;
    height: unset;
  }
`

export function App(): JSX.Element {
  return (
    <>
      <Global styles={globalStyles}/>
      <div css={appStyles}>
        <Router/>
      </div>
    </>
  )
}
