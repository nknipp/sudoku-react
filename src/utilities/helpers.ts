import { Cells } from '../types/cells'
import { CellPosition } from '../types'

export function getRandom(max: number) {
  return Math.floor(Math.random() * max)
}

export function getIndexFromCellPosition(pos: CellPosition) {
  return (
    Math.floor(pos.blockIndex / 3) * 27 +
    (pos.blockIndex % 3) * 3 +
    Math.floor(pos.cellIndex / 3) * 9 +
    (pos.cellIndex % 3)
  )
}

export function collectNeighbors(cells: Cells, pos: number) {
  const neighbors = Array(0)

  // collect column
  const colStart = pos % 9
  for (let idx = colStart; idx <= colStart + 72; idx += 9) {
    neighbors.push(cells.get(idx))
  }

  // collect row
  const rowStart = Math.floor(pos / 9) * 9
  for (let idx = rowStart; idx <= rowStart + 8; idx++) {
    neighbors.push(cells.get(idx))
  }

  // collect block
  const startRow = Math.floor(pos / 9 / 3) * 3
  const startCol = Math.floor((pos % 9) / 3) * 3
  for (let row = startRow; row < startRow + 3; row++) {
    for (let col = startCol; col < startCol + 3; col++) {
      neighbors.push(cells.get(row * 9 + col))
    }
  }

  return neighbors
}
