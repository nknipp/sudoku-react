import _ from 'lodash'
import { collectNeighbors, getRandom } from './helpers'
import { Cells } from '../types/cells'
import { CellState } from '../types'

let cells: Cells

export function shuffleBoard() {
  cells = new Cells()

  if (!fillCells(0)) {
    throw new Error('Unable to create board')
  }

  return cells
}

function fillCells(pos: number) {
  const neighbors = collectNeighbors(cells, pos).map(cell => cell.number)
  const options = _.difference([1, 2, 3, 4, 5, 6, 7, 8, 9], neighbors)
  shuffleOptions(options)

  for (const option of options) {
    cells.get(pos).number = option
    cells.get(pos).state = CellState.Fixed

    if (pos === cells.length - 1) {
      return true
    }

    if (fillCells(pos + 1)) {
      return true
    }
  }

  cells.get(pos).number = 0
  return false
}

function shuffleOptions(options: number[]) {
  if (options.length >= 2) {
    for (let i = 0; i < Math.ceil(options.length / 2); i++) {
      const first = getRandom(options.length)
      const second = getRandom(options.length)
      const swap = options[first]
      options[first] = options[second]
      options[second] = swap
    }
  }
}
