import { getRandom } from './helpers'
import { Cells } from '../types/cells'
import { CellState } from '../types'

export function graduate(cells: Cells, level: number): Cells {
  const numbersToRemove = (level + 1) * 12
  let setOfNumbers: Set<number> = new Set()

  while (setOfNumbers.size < numbersToRemove) {
    setOfNumbers = setOfNumbers.add(getRandom(80))
  }

  const graduatedCells = cells.clone()
  for (const idx of setOfNumbers) {
    graduatedCells.get(idx).number = null
    graduatedCells.get(idx).state &= ~CellState.Fixed
  }

  return graduatedCells
}
