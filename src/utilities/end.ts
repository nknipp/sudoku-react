import { CellState } from '../types'
import { Cells } from '../types/cells'

/**
 * Detect if cells are in state Collide and unfilled.
 */
export function detectEnd(cells: Cells): boolean {
  const collisions = cells.filter(cell => (cell.state & CellState.Collide) === CellState.Collide )
  const unfilled = cells.filter(cell => cell.number === null)
  return collisions.length === 0 && unfilled.length === 0
}
