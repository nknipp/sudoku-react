import { CellState } from '../types'
import { Cells } from '../types/cells'
import { collectNeighbors } from './helpers'

export function detectCollisions(cells: Cells): void {
  clearCollisions(cells)
  cells.forEach((cell, index) => detectCollision(cells, index))
}

function clearCollisions(cells: Cells): void {
  cells.forEach(cell => (cell.state &= ~CellState.Collide))
}

function detectCollision(cells: Cells, pos: number): void {
  const num = cells.get(pos).number
  const neighbors = collectNeighbors(cells, pos)
  neighbors.forEach(cell => {
    if (num !== null && cell.number === num && cells.indexOf(cell) !== pos) {
      cell.state |= CellState.Collide
    }
  })
}
