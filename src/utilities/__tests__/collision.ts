import { detectCollisions } from '../collision'
import { Cells } from '../../types/cells'
import { CellState } from '../../types'

//  0  1  2 |  3  4  5 |  6  7  8
//  9 10 11 | 12 13 14 | 15 16 17
// 18 19 20 | 21 22 23 | 24 25 26
// ------------------------------
// 27 28 29 | 30 31 32 | 33 34 35
// 36 37 38 | 39 40 41 | 42 43 44
// 45 46 47 | 48 49 50 | 51 52 53
// ------------------------------
// 54 55 56 | 57 58 59 | 60 61 62
// 63 64 65 | 66 67 68 | 69 70 71
// 72 73 74 | 75 76 77 | 78 79 80

let cells: Cells
beforeEach(() => {
  cells = new Cells()
  cells.forEach((cell, index) => (cell.number = index))
})

const filterCells = (c: Cells) => c.filter(
  cell => (cell.state & CellState.Collide) === CellState.Collide
)

test('find four collisions for index 40', () => {
  // define collisions
  cells.get(13).number = 40
  cells.get(36).number = 40
  cells.get(50).number = 40

  detectCollisions(cells)

  const collisions = filterCells(cells)

  expect(collisions.length).toBe(4)
})

test('find no collision', () => {
  detectCollisions(cells)

  const collisions = filterCells(cells)

  expect(collisions.length).toBe(0)
})

test('clear all collisions', () => {
  // define collisions
  cells.get(13).number = 40
  cells.get(36).number = 40
  cells.get(50).number = 40

  detectCollisions(cells)

  let collisions = filterCells(cells)

  expect(collisions.length).toBe(4)

  // reset collisions
  cells.get(13).number = 13
  cells.get(36).number = 36
  cells.get(50).number = 50

  detectCollisions(cells)

  collisions = filterCells(cells)

  expect(collisions.length).toBe(0)
})
