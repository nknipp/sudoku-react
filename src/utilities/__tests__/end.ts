import { detectEnd } from '../end'
import { detectCollisions } from '../collision'
import { Cells } from '../../types/cells'

let cells: Cells
beforeEach(() => {
  cells = new Cells()
  cells.forEach((cell, index) => (cell.number = index))
})

test('find unfilled cells', () => {
  // define unfilled cells
  cells.get(13).number = null
  cells.get(36).number = null
  cells.get(50).number = null

  const unfilled = detectEnd(cells)

  expect(unfilled).toBeFalsy()
})

test('find collisions', () => {
  // define collisions
  cells.get(13).number = 40
  cells.get(36).number = 40
  cells.get(50).number = 40
  detectCollisions(cells)

  const collisions = detectEnd(cells)

  expect(collisions).toBeFalsy()
})

test('find whether unfilled cells nor collisions', () => {
  const finished = detectEnd(cells)

  expect(finished).toBeTruthy()
})
