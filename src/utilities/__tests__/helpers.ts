import { Cells } from '../../types/cells'
import {
  collectNeighbors,
  getIndexFromCellPosition,
  getRandom
} from '../helpers'

describe('collect neighbors', () => {
  test('collect the neighbors from given index', () => {
    const cells = new Cells()

    //  0  1  2 |  3  4  5 |  6  7  8
    //  9 10 11 | 12 13 14 | 15 16 17
    // 18 19 20 | 21 22 23 | 24 25 26
    // ------------------------------
    // 27 28 29 | 30 31 32 | 33 34 35
    // 36 37 38 | 39 40 41 | 42 43 44
    // 45 46 47 | 48 49 50 | 51 52 53
    // ------------------------------
    // 54 55 56 | 57 58 59 | 60 61 62
    // 63 64 65 | 66 67 68 | 69 70 71
    // 72 73 74 | 75 76 77 | 78 79 80

    for (let idx = 0; idx < cells.length; idx++) {
      cells.get(idx).number = idx
    }

    const collectedCells = collectNeighbors(cells, 40)

    const collectedNumbers = collectedCells.map(cell => cell.number).sort()

    const expected = [
      13,
      22,
      30,
      31,
      31,
      32,
      36,
      37,
      38,
      39,
      39,
      4,
      40,
      40,
      40,
      41,
      41,
      42,
      43,
      44,
      48,
      49,
      49,
      50,
      58,
      67,
      76
    ]

    expect(collectedNumbers).toEqual(expected)
  })
})

describe('index from cell pos', () => {
  test('provide the index from a cell position', () => {
    let index = getIndexFromCellPosition({ blockIndex: 4, cellIndex: 4 })
    expect(index).toBe(40)

    index = getIndexFromCellPosition({ blockIndex: 0, cellIndex: 0 })
    expect(index).toBe(0)

    index = getIndexFromCellPosition({ blockIndex: 8, cellIndex: 8 })
    expect(index).toBe(80)
  })
})

describe('generate random number', () => {
  test('provide a random number', () => {
    let num = getRandom(1)
    expect(num).toBeGreaterThanOrEqual(0)
    expect(num).toBeLessThanOrEqual(1)

    for (let i = 0; i < 100; i++) {
      num = getRandom(10)
      expect(num).toBeGreaterThanOrEqual(0)
      expect(num).toBeLessThanOrEqual(10)
    }
  })
})
