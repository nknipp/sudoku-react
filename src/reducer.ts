import { CellPosition, CellState, GameState } from './types'
import { initialState, State } from './store'
import { graduate } from './utilities/graduate'
import { shuffleBoard } from './utilities/shuffle'
import { detectCollisions } from './utilities/collision'
import { detectEnd } from './utilities/end'
import { Cells } from './types/cells'

export interface Action {
  type: string
}

export interface StartNewGameAction extends Action {
  level: number
}

export interface ChangeGameStateAction extends Action {
  gameState: GameState
}

export interface SetCellNumberAction extends Action {
  cellPos: CellPosition
  number: number | null
}

export interface ToggleCellNoteAction extends Action {
  cellPos: CellPosition
  number: number
}

const START_NEW_GAME = 'START_NEW_GAME'

export function startNewGame(level: number): StartNewGameAction {
  return {
    type: START_NEW_GAME,
    level
  }
}

const CHANGE_GAME_STATE = 'CHANGE_GAME_STATE'

export function changeGameState(gameState: GameState): ChangeGameStateAction {
  return {
    type: CHANGE_GAME_STATE,
    gameState
  }
}

const SET_CELL_NUMBER = 'SET_CELL_NUMBER'

export function setCellNumber(cellPos: CellPosition, number: number | null): SetCellNumberAction {
  return {
    type: SET_CELL_NUMBER,
    cellPos,
    number
  }
}

const TOGGLE_CELL_NOTE = 'TOGGLE_CELL_NOTE'

export function toggleCellNote(cellPos: CellPosition, number: number): ToggleCellNoteAction {
  return {
    type: TOGGLE_CELL_NOTE,
    cellPos,
    number
  }
}

const UNDO_MOVE = 'UNDO_MOVE'

export function undoMove(): Action {
  return {
    type: UNDO_MOVE
  }
}

const REDO_MOVE = 'REDO_MOVE'

export function redoMove(): Action {
  return {
    type: REDO_MOVE
  }
}

function _setCellNumber(cells: Cells, action: Action): void {
  const cell = cells.getCell((action as SetCellNumberAction).cellPos)
  cell.number = (action as SetCellNumberAction).number
}

function _toggleCellNote(cells: Cells, action: Action): void {
  const cell = cells.getCell((action as ToggleCellNoteAction).cellPos)
  const number = (action as ToggleCellNoteAction).number
  if (cell.notes.has(number)) {
    cell.notes.delete(number)
  } else {
    cell.notes.add(number)
  }
}

function _resetMoves(state: State, currentAction: Action): Array<Action> {
  if (state.movesIndex > 0) {
    return state.moves.slice(0, state.moves.length - state.movesIndex).concat(currentAction)
  } else {
    return [...state.moves].concat(currentAction)
  }
}

function _clearMoves(cells: Cells): void {
  cells.forEach(cell => {
    if (cell.state !== CellState.Fixed) {
      cell.state = CellState.None
      cell.number = null
      cell.notes.clear()
    }
  })
}

export function reducer(state: State | undefined = initialState, action: Action): State {
  if (action.type === START_NEW_GAME) {
    return {
      cells: graduate(shuffleBoard(), (action as StartNewGameAction).level),
      gameState: GameState.Running,
      moves: [],
      movesIndex: 0
    }
  } else if (action.type === CHANGE_GAME_STATE) {
    return {
      cells: state.cells.clone(),
      gameState: (action as ChangeGameStateAction).gameState,
      moves: state.moves,
      movesIndex: state.movesIndex
    }
  } else if (action.type === SET_CELL_NUMBER) {
    const newCells = state.cells.clone()
    _setCellNumber(newCells, action)
    detectCollisions(newCells)
    const gameState = detectEnd(newCells) ? GameState.Finished : state.gameState
    return {
      cells: newCells,
      gameState,
      moves: _resetMoves(state, action),
      movesIndex: 0
    }
  } else if (action.type === TOGGLE_CELL_NOTE) {
    const newCells = state.cells.clone()
    _toggleCellNote(newCells, action)
    return {
      cells: newCells,
      gameState: state.gameState,
      moves: _resetMoves(state, action),
      movesIndex: 0
    }
  } else if (action.type === UNDO_MOVE) {
    const newCells = state.cells.clone()
    const newMovesIndex = state.movesIndex + 1
    _clearMoves(newCells)
    for (let idx = 0; idx < state.moves.length - newMovesIndex; idx++) {
      const move = state.moves[idx]
      if (move.type === SET_CELL_NUMBER) {
        _setCellNumber(newCells, move)
      } else if (move.type === TOGGLE_CELL_NOTE) {
        _toggleCellNote(newCells, move)
      }
    }
    detectCollisions(newCells)
    return {
      cells: newCells,
      gameState: state.gameState,
      moves: state.moves,
      movesIndex: newMovesIndex
    }
  } else if (action.type === REDO_MOVE) {
    const newCells = state.cells.clone()
    const move = state.moves[state.moves.length - state.movesIndex]
    const newMovesIndex = state.movesIndex - 1
    if (move.type === SET_CELL_NUMBER) {
      _setCellNumber(newCells, move)
    } else if (move.type === TOGGLE_CELL_NOTE) {
      _toggleCellNote(newCells, move)
    }
    return {
      cells: newCells,
      gameState: state.gameState,
      moves: state.moves,
      movesIndex: newMovesIndex
    }
  }
  return state
}
