import React, { ChangeEvent, useState } from 'react'
import { startNewGame } from '../reducer'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'

export function Game(): JSX.Element {
  function start() {
    dispatch(startNewGame(parseInt(level)))
    history.push('/board')
  }

  const [level,setLevel] = useState('2')
  const dispatch = useDispatch()
  const history = useHistory()

  return (
    <section className="container">
      <div>
        <button onClick={start}>Start new game</button>
        <select
          value={level}
          onChange={(e: ChangeEvent<HTMLSelectElement>) => setLevel(e.target.value)}
        >
          <option value="0">Very easy</option>
          <option value="1">Easy</option>
          <option value="2">Middle</option>
          <option value="3">Hard</option>
        </select>
      </div>
    </section>
  )
}
