import React from 'react'

export function Finished(): JSX.Element {
  return (
    <section className="container">
      <div>
        <h1>Congratulation!</h1>
        <p>You have successfully completed the game.</p>
      </div>
    </section>
  )
}
