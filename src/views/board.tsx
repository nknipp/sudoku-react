import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { css } from '@emotion/core'
import { changeGameState, redoMove, undoMove } from '../reducer'
import { selectCanRedo, selectCanUndo, selectGameState } from '../store'
import { GameState } from '../types'
import { DisabledLink } from '../components/disabledLink'
import { Uptime } from '../components/uptime'
import { Cell } from '../components/cell'

const containerStyles = css`
  display: grid;
  grid-gap: 20px;

  /* default: (orientation: portrait) */
  grid-template-columns: 1fr;
  grid-template-rows: repeat(1, 1fr);

  .uptimeBlock,
  .boardBlock,
  .controlBlock {
    justify-self: center;
    align-self: center;
  }

  @media (orientation: landscape) and (max-width: 1023px) {
    grid-template-columns: var(--board-dimension) 250px;
    grid-template-rows: repeat(2, 1fr);
    
    justify-content: center;

    .uptimeBlock {
      grid-column: 2;
      grid-row: 1;

      align-self: flex-start;
    }
    
    .boardBlock {
      grid-row: 1 / 3;
    }
    
    .controlBlock {
      grid-column: 2;
      grid-row: 2;
      
      align-self: flex-end;
    }
  }
`

const boardStyles = css`
  width: var(--board-dimension);
  height: var(--board-dimension);
  
  position: relative;

  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(3, 1fr);
  
  border: 3px solid darkgray;
  cursor: pointer;
`

const blockStyles = css`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(3, 1fr);
  
  border: 2px solid darkgray;
`

export function Board(): JSX.Element {
  function cancel() {
    dispatch(changeGameState(GameState.Halted))
  }

  function pause() {
    dispatch(changeGameState(GameState.Paused))
  }

  function resume() {
    dispatch(changeGameState(GameState.Running))
  }

  function undo() {
    dispatch(undoMove())
  }

  function redo() {
    dispatch(redoMove())
  }

  const dispatch = useDispatch()
  const gameState = useSelector(selectGameState)
  const canUndo = useSelector(selectCanUndo)
  const canRedo = useSelector(selectCanRedo)

  const isPaused = gameState === GameState.Paused
  const isRunning = gameState === GameState.Running
  // const isFinished = gameState === GameState.Finished
  const history = useHistory()

  useEffect(() => {
    if (gameState === GameState.Finished) {
      history.push('/finished')
    } else if (gameState === GameState.Halted) {
      history.push('/')
    }
  }, [gameState, history])

  let component
  if (isRunning) {
    component =
      <div css={boardStyles}>
        {[0, 1, 2, 3, 4, 5, 6, 7, 8].map(blockIndex =>
          <div key={blockIndex} css={blockStyles}>
            {[0, 1, 2, 3, 4, 5, 6, 7, 8].map(i => <Cell key={i} cellPos={{ blockIndex, cellIndex: i }}/>)}
          </div>
        )}
      </div>

  } else if (isPaused) {
    component = <div className="pause-placeholder">Game paused</div>
  }

  return (
    <section css={containerStyles}>
        <div className="uptimeBlock">
          <Uptime/>
        </div>

        <div className="boardBlock">
          {component}
        </div>

        <div className="controlBlock">
          <DisabledLink
            disable={isPaused}
            click={pause}
            title="Pause game"
            icon="pause_circle_outline"
          />

          <DisabledLink
            disable={isRunning}
            click={resume}
            title="Resume game"
            icon="play_circle_outline"
          />

          <DisabledLink
            disable={false}
            click={cancel}
            title="Cancel game"
            icon="highlight_off"
          />

          <DisabledLink
            disable={!canUndo}
            click={undo}
            title="Undo"
            icon="undo"
          />

          <DisabledLink
            disable={!canRedo}
            click={redo}
            title="Redo"
            icon="redo"
          />
        </div>
    </section>
  )
}
