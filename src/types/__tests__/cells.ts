import { Cells } from '../cells'
import { CellState } from '../../types'

test('clone cells', () => {
  const originalCells = new Cells()
  for (let idx = 0; idx < originalCells.length; idx++) {
    originalCells.get(idx).number = idx % 9 + 1
    originalCells.get(idx).state = CellState.Fixed
  }

  const clonedCells = originalCells.clone()

  expect(clonedCells).toEqual(originalCells)
  expect(clonedCells).not.toBe(originalCells)
  expect(clonedCells.get(0)).not.toBe(originalCells.get(0))
})
