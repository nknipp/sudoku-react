import { Cell } from '../cell'
import { CellState } from '../../types'

test('clone cell', () => {
  const originalCell = new Cell()
  originalCell.number = 5
  originalCell.notes.add(5)
  originalCell.state = CellState.Fixed

  const clonedCell = originalCell.clone()

  expect(clonedCell).toEqual(originalCell)
  expect(clonedCell).not.toBe(originalCell)
})
