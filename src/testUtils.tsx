import React, { PropsWithChildren } from 'react'
import { createStore } from '@reduxjs/toolkit'
import { Provider } from 'react-redux'
import { ThemeProvider } from 'emotion-theming'
import { render, RenderOptions, RenderResult } from '@testing-library/react'
import { reducer } from './reducer'
import { theme } from './theme'
import { State } from './store'
import { Cells } from './types/cells'
import { GameState } from './types'

const ProviderWrapper = (state: State) => {
  const store = createStore(reducer, state)
  return ({ children }: PropsWithChildren<unknown>) => {
    return (
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          {children}
        </ThemeProvider>
      </Provider>
    )
  }
}

const customRender = (ui: React.ReactElement, state?: State, options?: Omit<RenderOptions, 'queries'>): RenderResult => {
  const stateUnderTest = state || {
    cells: new Cells(),
    gameState: GameState.Running,
    moves: [],
    movesIndex: 0
  }
  const providerWrapper = ProviderWrapper(stateUnderTest)
  return render(ui, { wrapper: providerWrapper, ...options })
}

export * from '@testing-library/react'
export * from './store'
export * from './reducer'
export { customRender as render }
