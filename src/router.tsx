import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Game } from './views/game'
import { Board } from './views/board'
import { Finished } from './views/finished'

export function Router() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/board">
          <Board/>
        </Route>
        <Route path="/finished">
          <Finished/>
        </Route>
        <Route path="/">
          <Game/>
        </Route>
      </Switch>
    </BrowserRouter>
  )
}
