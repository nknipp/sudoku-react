import React, { useState } from 'react'
import { css } from '@emotion/core'
import { CellPosition, CellState } from '../types'
import { NumberPanel } from './numberPanel'
import { shallowEqual, useDispatch, useSelector } from 'react-redux'
import { setCellNumber, toggleCellNote } from '../reducer'
import { selectCells } from '../store'

const cellStyles = css`
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1.6em;
  border: 1px solid darkgray;

  &.fixed {
    background-color: lightgray;
  }

  &.collide {
    color: red;
  }

  .notes {
    display: flex;
    flex-flow: row wrap;
    font-size: 0.35em;

    div {
      flex: 0 0 33%;
    }
  }
`

export function Cell({ cellPos }: { cellPos: CellPosition }): JSX.Element {
  function clicked() {
    if (!isFixed) {
      setShowNumberPanel(true)
    }
  }

  function numberChosen(number: number) {
    if (notesMode) {
      dispatch(toggleCellNote(cellPos, number))
    } else {
      dispatch(setCellNumber(cellPos, number))
    }
    setShowNumberPanel(false)
  }

  function toggleNotesMode() {
    setNodesMode(notesMode => !notesMode)
  }

  function clear() {
    dispatch(setCellNumber(cellPos, null))
    setShowNumberPanel(false)
  }

  function close() {
    setShowNumberPanel(false)
  }

  const [showNumberPanel, setShowNumberPanel] = useState(false)
  const [notesMode, setNodesMode] = useState(false)
  const cells = useSelector(selectCells, shallowEqual)
  const dispatch = useDispatch()

  const thisCell = cells.getCell({
    blockIndex: cellPos.blockIndex,
    cellIndex: cellPos.cellIndex
  })
  const isFixed = (thisCell.state & CellState.Fixed) === CellState.Fixed
  const isCollision = (thisCell.state & CellState.Collide) === CellState.Collide

  let child, classes = ''
  if (thisCell.number !== null) {
    classes = isFixed ? 'fixed ' : ''
    classes += isCollision ? ' collide' : ''
    child = <div>{thisCell.number}</div>
  } else {
    child = <div className="notes">
      {[1, 2, 3, 4, 5, 6, 7, 8, 9].map(i => <div key={i}>{thisCell.notes.has(i) ? i : '\u00A0'}</div>)}
    </div>
  }

  return (
    <>
      {showNumberPanel
        ? <NumberPanel cellPos={cellPos}
                       notesMode={notesMode}
                       numberChosen={numberChosen}
                       clear={clear}
                       close={close}
                       toggleNotesMode={toggleNotesMode}/>
        : null
      }
      <div data-testid={`cell-${cellPos.blockIndex}-${cellPos.cellIndex}`} onClick={clicked} className={classes} css={cellStyles}>
        {child}
      </div>
    </>
  )
}
