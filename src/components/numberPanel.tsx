import React, { EventHandler } from 'react'
import { css } from '@emotion/core'
import { CellPosition } from '../types'

const numberPanelStyles = css`
  display: contents;
  
  .panel {
    position: absolute;
    width: 124px;
    height: 156px;

    background-color: white;
    border: 2px solid darkgray;
    
    .block {
      display: grid;
      grid-template-columns: repeat(3, 40px);
      grid-template-rows: repeat(3, 40px);
    }
  
    .cell {
      display: flex;
      justify-content: center;
      align-items: center;
      
      border: 1px solid darkgray;
    }
  
    .controls {
      display: flex;
      justify-content: space-around;
      align-items: center;
      
      height: 32px;
      border-top: 1px solid darkgray;
    
      .edit {
        background-color: black;
        color: white;
      }
    }
  }
  
  .modal {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    
    background-color: #f9f9f9;
    opacity: 0.7;
  }
`

interface NumberPanelProps {
  cellPos: CellPosition
  notesMode: boolean
  numberChosen: EventHandler<any>
  toggleNotesMode: EventHandler<any>
  clear: EventHandler<any>
  close: EventHandler<any>
}

export function NumberPanel({ cellPos, numberChosen, clear, close, toggleNotesMode, notesMode }: NumberPanelProps): JSX.Element {
  function calcRow(row: number): number {
    return row < 3 ? 0 : row < 6 ? 1 : 2
  }

  function calcCol(col: number): number {
    return col % 3
  }

  const boardDimension = document.getElementsByClassName('boardBlock')[0].clientWidth
  const panelWidth = 124
  const panelHeight = 156

  const boardCellSize = 40
  const boardCellBorderWidth = 1
  const blockBorderWidth = 2

  const cellSize = (boardCellSize + 2 * boardCellBorderWidth)
  const blockSize = 3 * cellSize + 2 * blockBorderWidth
  const blockRow = calcRow(cellPos.blockIndex)
  const blockCol = calcCol(cellPos.blockIndex)
  const cellRow = calcRow(cellPos.cellIndex)
  const cellCol = calcCol(cellPos.cellIndex)

  let top = blockRow * blockSize + cellRow * cellSize - cellSize
  let left = blockCol * blockSize + cellCol * cellSize - cellSize

  if (top < 0) top = 0
  else if (top + panelHeight > boardDimension) top = boardDimension - panelHeight - 3
  if (left < 0) left = 0
  else if (left + panelWidth > boardDimension) left = boardDimension - panelWidth - 3

  return (
    <div data-testid="number-panel" css={numberPanelStyles}>
      <div className="modal"/>
      <div className="panel"
           style={{ top: top, left: left }}>
        <div className="block">
          {[1, 2, 3, 4, 5, 6, 7, 8, 9].map(i =>
            <div
              key={i}
              className="cell"
              onClick={() => numberChosen(i)}
            >
              {i}
            </div>)
          }
        </div>
        <div className="controls">
          <a onClick={clear} title="Clear field"><i className="material-icons">delete</i></a>
          <a onClick={close} title="Cancel"><i className="material-icons">cancel</i></a>
          <a onClick={toggleNotesMode} title="Notes">
            <i className={notesMode ? 'material-icons edit' : 'material-icons'}>apps</i>
          </a>
        </div>
      </div>
    </div>
  )
}
