import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import {css} from '@emotion/core'
import { selectGameState } from '../store'
import { GameState } from '../types'

const uptimeStyles = css`
  font-family: 'Digital Numbers Regular';
  font-weight: normal;
  font-size: 42px
`

export function Uptime() {
  function formatNumber(type: string) {
    let value = 0
    if (type === 'minutes') {
      value = Math.floor(uptime / 60)
    } else if (type === 'seconds') {
      value = uptime % 60
    }
    return value < 10 ? `0${value}` : value
  }

  const [uptime, setUptime] = useState(0)
  const gameState = useSelector(selectGameState)

  useEffect(() => {
    let timerId: any
    if (gameState === GameState.Running) {
      timerId = setInterval(() => {
        setUptime(uptime => uptime + 1)
      }, 1000)
    } else if (gameState === GameState.Paused || gameState === GameState.Halted) {
      clearInterval(timerId)
    }
    return () => {
      clearInterval(timerId)
    }
  }, [gameState])

  return (
    <div css={uptimeStyles}>
      <span>{formatNumber('minutes')}:{formatNumber('seconds')}</span>
    </div>
  )
}
