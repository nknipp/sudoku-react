import React from 'react'
import { fireEvent, getByText, render, screen, State } from '../../testUtils'
import { Cell } from '../cell'
import { CellState, GameState } from '../../types'
import { Cells } from '../../types/cells'

let state: State
beforeEach(() => {
  state = {
    cells: new Cells(),
    gameState: GameState.Running,
    moves: [],
    movesIndex: 0
  }

  const boardBlock = document.createElement('div')
  boardBlock.className = 'boardBlock'
  document.body.appendChild(boardBlock)
})

test('choose number in empty cell', () => {
  render(<Cell cellPos={{ blockIndex: 0, cellIndex: 0}} />)

  fireEvent.click(screen.getByTestId('cell-0-0'))
  fireEvent.click(screen.getByText('6'))

  expect(screen.queryByTestId('number-panel')).toBeNull()
  expect(screen.getByTestId('cell-0-0')).toHaveTextContent('6')
})

test('click into fixed cell', () => {
  const cell = state.cells.get(0)
  cell.number = 4
  cell.state = CellState.Fixed

  render(<Cell cellPos={{ blockIndex: 0, cellIndex: 0}} />, state)

  fireEvent.click(screen.getByTestId('cell-0-0'))

  expect(screen.queryByTestId('number-panel')).toBeNull()
  expect(screen.getByTestId('cell-0-0')).toHaveTextContent('4')
})

test('click into filled cell and enter another number', () => {
  const cell = state.cells.get(0)
  cell.number = 4
  cell.state = CellState.None

  render(<Cell cellPos={{ blockIndex: 0, cellIndex: 0}} />, state)

  fireEvent.click(screen.getByTestId('cell-0-0'))
  fireEvent.click(screen.getByText('6'))

  expect(screen.getByTestId('cell-0-0')).toHaveTextContent('6')
})

test('click into empty cell and enter several notes', () => {
  render(<Cell cellPos={{ blockIndex: 0, cellIndex: 0}} />)

  fireEvent.click(screen.getByTestId('cell-0-0'))
  fireEvent.click(screen.getByTitle('Notes'))
  fireEvent.click(screen.getByText('6'))

  fireEvent.click(screen.getByTestId('cell-0-0'))
  fireEvent.click(screen.getByText('2'))

  fireEvent.click(screen.getByTestId('cell-0-0'))
  fireEvent.click(screen.getByText('9'))

  expect(screen.getByTestId('cell-0-0')).toHaveTextContent('2 6 9')
})

test('click into filled cell and enter several notes', () => {
  const cell = state.cells.get(0)
  cell.number = 4
  cell.state = CellState.None

  render(<Cell cellPos={{ blockIndex: 0, cellIndex: 0}} />, state)

  fireEvent.click(screen.getByTestId('cell-0-0'))
  fireEvent.click(screen.getByTitle('Notes'))
  fireEvent.click(screen.getByText('6'))

  fireEvent.click(screen.getByTestId('cell-0-0'))
  fireEvent.click(screen.getByText('2'))

  fireEvent.click(screen.getByTestId('cell-0-0'))
  fireEvent.click(screen.getByText('9'))

  expect(screen.getByTestId('cell-0-0')).toHaveTextContent('4')
})

test('click into filled cell, enter several notes then clear field', () => {
  const cell = state.cells.get(0)
  cell.number = 4
  cell.state = CellState.None

  render(<Cell cellPos={{ blockIndex: 0, cellIndex: 0}} />, state)

  fireEvent.click(screen.getByTestId('cell-0-0'))
  fireEvent.click(screen.getByTitle('Notes'))
  fireEvent.click(screen.getByText('1'))

  fireEvent.click(screen.getByTestId('cell-0-0'))
  fireEvent.click(screen.getByText('5'))

  fireEvent.click(screen.getByTestId('cell-0-0'))
  fireEvent.click(screen.getByText('8'))

  expect(screen.getByTestId('cell-0-0')).toHaveTextContent('4')

  fireEvent.click(screen.getByTestId('cell-0-0'))
  fireEvent.click(screen.getByTitle('Clear field'))

  expect(screen.getByTestId('cell-0-0')).toHaveTextContent('1 5 8')
})

test('click into empty field, enter and remove a notes', () => {
  render(<Cell cellPos={{ blockIndex: 0, cellIndex: 0}} />)

  fireEvent.click(screen.getByTestId('cell-0-0'))
  fireEvent.click(screen.getByTitle('Notes'))
  fireEvent.click(screen.getByText('1'))

  fireEvent.click(screen.getByTestId('cell-0-0'))
  fireEvent.click(getByText(screen.getByTestId('number-panel'), '1'))

  expect(screen.getByTestId('cell-0-0')).toHaveTextContent('')
})
