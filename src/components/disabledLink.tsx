import React, { MouseEventHandler } from 'react'
import { css } from '@emotion/core'

const controlStyles = css`
  &.control {
    cursor: pointer;
    outline: none;
    background-color: transparent;
    border: none;
  }
  
  &.disabled {
    pointer-events: none;
    color: lightgray;
  }
  
  .material-icons {
    font-size: 32px;
  }
`

interface DisabledLinkProps {
  disable: boolean
  click: MouseEventHandler<any>
  title: string
  icon: string
}

export function DisabledLink({ click, disable, icon, title }: DisabledLinkProps): JSX.Element {
  const classes = `control ${disable ? 'disabled' : ''}`
  return (
    <button
      css={controlStyles}
      className={classes}
      onClick={click}
      title={title}
    >
      <i className="material-icons">{icon}</i>
    </button>
  )
}
