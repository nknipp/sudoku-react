import { createStore } from '@reduxjs/toolkit'
import { Action, reducer } from './reducer'
import { GameState } from './types'
import { Cells } from './types/cells'

export interface State {
  cells: Cells
  gameState: GameState
  moves: Array<Action>
  movesIndex: number
}

export const initialState: State = {
  cells: new Cells(),
  gameState: GameState.Halted,
  moves: [],
  movesIndex: 0
}

export const store = createStore(reducer, initialState, (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__())

export const selectCells = (state: State): Cells => state.cells
export const selectGameState = (state: State): GameState => state.gameState
export const selectCanUndo = (state: State): boolean => state.moves.length > 0 && state.moves.length - state.movesIndex > 0
export const selectCanRedo = (state: State): boolean => state.movesIndex > 0
