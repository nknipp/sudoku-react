import { Cells } from './types/cells'

export interface CellPosition {
  blockIndex: number
  cellIndex: number
}

export enum CellState {
  None = 0,
  Fixed = 1,
  Collide = 2
}

export enum GameState {
  Halted,
  Running,
  Paused,
  Finished
}

export interface StateType {
  cells: Cells
  gameState: GameState
}
