import { redoMove, reducer, setCellNumber, startNewGame, toggleCellNote, undoMove } from '../reducer'
import { Cells } from '../types/cells'
import { CellState, GameState } from '../types'
import { State } from '../store'

describe('moves and movesIndex', () => {
  test('initial state', () => {
    const action = startNewGame(1)

    const state = reducer(undefined, action)

    expect(state.moves.length).toBe(0)
    expect(state.movesIndex).toBe(0)
  })

  test('insert a number', () => {
    const action = setCellNumber({ cellIndex: 1, blockIndex: 1 }, 5)

    const state = reducer(undefined, action)

    expect(state.moves.length).toBe(1)
    expect(state.movesIndex).toBe(0)
    expect(state.moves[0]).toEqual(action)
  })

  test('insert a note', () => {
    const action = toggleCellNote({ cellIndex: 1, blockIndex: 1 }, 6)

    const state = reducer(undefined, action)

    expect(state.moves.length).toBe(1)
    expect(state.movesIndex).toBe(0)
    expect(state.moves[0]).toEqual(action)
  })

  test('insert several numbers and notes', () => {
    const action1 = setCellNumber({ cellIndex: 1, blockIndex: 1 }, 5)
    const action2 = setCellNumber({ cellIndex: 2, blockIndex: 1 }, 6)
    const action3 = setCellNumber({ cellIndex: 3, blockIndex: 1 }, 7)
    const action4 = toggleCellNote({ cellIndex: 4, blockIndex: 1 }, 8)
    const action5 = setCellNumber({ cellIndex: 5, blockIndex: 1 }, 9)

    let state = reducer(undefined, action1)
    state = reducer(state, action2)
    state = reducer(state, action3)
    state = reducer(state, action4)
    state = reducer(state, action5)

    expect(state.moves.length).toBe(5)
    expect(state.movesIndex).toBe(0)
    expect(state.moves[0]).toEqual(action1)
    expect(state.moves[1]).toEqual(action2)
    expect(state.moves[2]).toEqual(action3)
    expect(state.moves[3]).toEqual(action4)
    expect(state.moves[4]).toEqual(action5)
  })
})

describe('undo and redo', () => {
  let initialState: State

  beforeEach(() => {
    const cells = new Cells()
    for (let idx = 0; idx < 9; idx++) {
      cells.get(idx).number = idx % 9 + 1
      cells.get(idx).state = CellState.Fixed
    }

    initialState = {
      cells,
      gameState: GameState.Running,
      moves: [],
      movesIndex: 0
    }
  })

  test('undo all and redo all', () => {
    let stateMoves = reducer(initialState, setCellNumber({ cellIndex: 7, blockIndex: 5 }, 5))
    stateMoves = reducer(stateMoves, setCellNumber({ cellIndex: 8, blockIndex: 5 }, 1))
    stateMoves = reducer(stateMoves, toggleCellNote({ cellIndex: 2, blockIndex: 6 }, 3))
    stateMoves = reducer(stateMoves, toggleCellNote({ cellIndex: 6, blockIndex: 7 }, 7))
    stateMoves = reducer(stateMoves, setCellNumber({ cellIndex: 8, blockIndex: 2 }, 6))

    let stateUndo = reducer(stateMoves, undoMove())
    stateUndo = reducer(stateUndo, undoMove())
    stateUndo = reducer(stateUndo, undoMove())
    stateUndo = reducer(stateUndo, undoMove())
    stateUndo = reducer(stateUndo, undoMove())
    expect(stateUndo.cells).toEqual(initialState.cells)
    expect(stateUndo.movesIndex).toBe(5)
    expect(stateUndo.moves.length).toBe(5)

    let stateRedo = reducer(stateUndo, redoMove())
    stateRedo = reducer(stateRedo, redoMove())
    stateRedo = reducer(stateRedo, redoMove())
    stateRedo = reducer(stateRedo, redoMove())
    stateRedo = reducer(stateRedo, redoMove())

    expect(stateRedo).toEqual(stateMoves)
    expect(stateRedo.movesIndex).toBe(0)
    expect(stateRedo.moves.length).toBe(5)
  })

  test('undo, set number and clear redo', () => {
    let state = reducer(initialState, setCellNumber({ cellIndex: 7, blockIndex: 5 }, 5))
    state = reducer(state, setCellNumber({ cellIndex: 8, blockIndex: 5 }, 1))
    state = reducer(state, toggleCellNote({ cellIndex: 2, blockIndex: 6 }, 3))
    state = reducer(state, toggleCellNote({ cellIndex: 6, blockIndex: 7 }, 7))
    state = reducer(state, setCellNumber({ cellIndex: 8, blockIndex: 2 }, 6))

    state = reducer(state, undoMove())
    state = reducer(state, undoMove())
    state = reducer(state, undoMove())
    state = reducer(state, setCellNumber({ cellIndex: 8, blockIndex: 2 }, 6))

    expect(state.moves.length).toBe(3)
    expect(state.movesIndex).toBe(0)
  })
})
